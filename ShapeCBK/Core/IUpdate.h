﻿#pragma once

namespace CBK
{
	class IUpdate
	{
	public:
		virtual ~IUpdate() = default;
		virtual void OnUpdate(float deltaTime) const = 0;
	};
}