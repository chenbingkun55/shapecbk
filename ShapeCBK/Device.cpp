#include "Device.h"
#include "GameConst.h"

namespace CBK
{
	Device::Device(emDeviceType emType, emDeviceDir emDir, Position pos, int nHP)
		: m_emType(emType), m_emDir(emDir), LifeEntity(pos, LifeEntity::LF_DEVICE, nHP)
	{
	}
	Device::~Device()
	{
	}
	Device::emDeviceType Device::getType()
	{
		return m_emType;
	}
	Device::emDeviceDir Device::getDir()
	{
		return m_emDir;
	}
	void Device::setType(emDeviceType type)
	{
		m_emType = type;
	}
	bool Device::onHitBy(int nDamage)
	{
		if (SubHp(nDamage) == false)
		{
			//清除显示
			ClearThis();

			//调用死亡
			toDie();
			return true;
		}
		else
		{
			show();
			g_Ui->flush();
		}

		return false;
	}
	void Device::ClearThis()
	{
		g_Ui->ClearRect(m_pos.getX(), m_pos.getY(), TILE_WIDTH, TILE_HEIGHT);
	}
	void Device::show()
	{
		g_Ui->DrawDevice(m_pos.getX(), m_pos.getY(), m_emType, m_emDir);
	}

	void Device::work()
	{
		switch (m_emType)
		{
		case CBK::Device::DEV_MINER:
		{
			Tile* tile = g_Game->GetTile(m_pos);
			if (tile == nullptr)
				return;

			Resource* pRes = tile->GetResource();
			if (pRes == nullptr)
				return;

			auto dataPos = tile->GetDataPos();
			printf("开采器: [%d,%d]位置, [%s]资源己开采完成.\r\n", dataPos.getX(), dataPos.getY(), pRes->GetName().c_str());

			// 己开采
			vecWorkRes.push_back(pRes);
			tile->MinerResource();
			tile->show();
		}
			break;
		case CBK::Device::DEV_BELT:
		{
			Tile* tile = g_Game->GetTile(m_pos);
			if (tile == nullptr)
				return;

			Device* pDev = nullptr;
			auto dataPos = tile->GetDataPos();
			// 上下传送
			if (m_emDir == emDeviceDir::TOP)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() +1);
				BeltResource(preTile, tile);
				preTile = g_Game->GetTile(dataPos.getX() - 1, dataPos.getY());
				BeltResource(preTile, tile);
				preTile = g_Game->GetTile(dataPos.getX() + 1, dataPos.getY());
				BeltResource(preTile, tile);
			}

			if (m_emDir == emDeviceDir::DOWN)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() -1);
				BeltResource(preTile, tile);
				preTile = g_Game->GetTile(dataPos.getX() -1, dataPos.getY());
				BeltResource(preTile, tile);
				preTile = g_Game->GetTile(dataPos.getX() + 1, dataPos.getY());
				BeltResource(preTile, tile);
			}

			// 左右传送
			if (m_emDir == emDeviceDir::LEFT)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX() +1, dataPos.getY());
				BeltResource(preTile, tile);
			}

			if (m_emDir == emDeviceDir::RIGHT)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX()-1, dataPos.getY());
				BeltResource(preTile, tile);
			}
		}
			break;
		case CBK::Device::DEV_CUTTER:
		{
			Tile* tile = g_Game->GetTile(m_pos);
			if (tile == nullptr)
				return;

			Device* pDev = nullptr;
			auto dataPos = tile->GetDataPos();
			// 上下电锯
			if (m_emDir == emDeviceDir::TOP)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() - 1);
				CutterResource(preTile, tile, false);
			}

			if (m_emDir == emDeviceDir::DOWN)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() + 1);
				CutterResource(preTile, tile, false);
			}

			// 左右电锯
			if (m_emDir == emDeviceDir::LEFT)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX() - 1, dataPos.getY());
				CutterResource(preTile, tile, true);
			}

			if (m_emDir == emDeviceDir::RIGHT)
			{
				Tile* preTile = g_Game->GetTile(dataPos.getX() + 1, dataPos.getY());
				CutterResource(preTile, tile, true);
			}
		}
		break;
		case CBK::Device::DEV_TRASH:
			break;
		case CBK::Device::DEV_CENTER:
		{
			Tile* tile = g_Game->GetTile(m_pos);
			if (tile == nullptr)
				return;

			Device* pDev = nullptr;
			auto dataPos = tile->GetDataPos();

			// 中心接收周边资源
			Tile* topTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() + 2);
			AddResourceScore(topTile);
			Tile* downTile = g_Game->GetTile(dataPos.getX(), dataPos.getY() - 2);
			AddResourceScore(downTile);
			Tile* leftTile = g_Game->GetTile(dataPos.getX() - 2, dataPos.getY());
			AddResourceScore(leftTile);
			Tile* rightTile = g_Game->GetTile(dataPos.getX() + 2, dataPos.getY());
			AddResourceScore(rightTile);
		}
			break;
		case CBK::Device::DEV_BUTTON:
			break;
		default:
			break;
		}
	}

	void Device::BeltResource(Tile* preTile, Tile* myTile)
	{
		if (preTile == nullptr || myTile == nullptr)
			return;
		Device* preDev = preTile->GetDevice();
		if (preDev == nullptr)
			return;
		if (preDev->vecWorkRes.size() <= 0)
			return;

		for (auto it = preDev->vecWorkRes.begin(); it != preDev->vecWorkRes.end(); it++)
		{
			auto dataPos = preTile->GetDataPos();
			auto myDataPos = myTile->GetDataPos();

			printf("传送带: 己将[%s]资源从[%d,%d]->[%d,%d]位置.\r\n", (*it)->GetName().c_str(), dataPos.getX(), dataPos.getY(), myDataPos.getX(), myDataPos.getY());
			vecWorkRes.push_back(*it);
		}

		preDev->vecWorkRes.clear();
	}

	void Device::CutterResource(Tile* preTile, Tile* myTile, bool leftRight)
	{
		if (preTile == nullptr || myTile == nullptr)
			return;
		Device* preDev = preTile->GetDevice();
		if (preDev == nullptr)
			return;
		if (preDev->vecWorkRes.size() <= 0)
			return;

		auto dataPos = myTile->GetDataPos();
		if (leftRight)
		{
			Tile* tile1 = g_Game->GetTile(dataPos.getX(), dataPos.getY() + 1);
			Tile* tile2 = g_Game->GetTile(dataPos.getX(), dataPos.getY() - 1);

			auto pDev1 = tile1->GetDevice();
			auto pDev2 = tile1->GetDevice();
			if ((pDev1 == nullptr || pDev1->getType() != emDeviceType::DEV_TRASH) || (pDev2 == nullptr || pDev2->getType() != emDeviceType::DEV_TRASH))
			{
				printf("垃圾桶: [%d,%d]位置的切割机, 旁边没找到垃圾桶, 己停止工作.\r\n", dataPos.getX(), dataPos.getY());
				return;
			}
		}
		else
		{
			Tile* tile1 = g_Game->GetTile(dataPos.getX() +1, dataPos.getY());
			Tile* tile2 = g_Game->GetTile(dataPos.getX() -1, dataPos.getY());

			auto pDev1 = tile1->GetDevice();
			auto pDev2 = tile1->GetDevice();
			if ((pDev1 == nullptr || pDev1->getType() != emDeviceType::DEV_TRASH) || (pDev2 == nullptr || pDev2->getType() != emDeviceType::DEV_TRASH))
			{
				printf("垃圾桶: [%d,%d]位置的切割机, 旁边没找到垃圾桶, 己停止工作.\r\n", dataPos.getX(), dataPos.getY());
				return;
			}
		}

		for (auto it = preDev->vecWorkRes.begin(); it != preDev->vecWorkRes.end(); it++)
		{
			if ((*it)->getType() != Resource::emResourceType::RES_DIAMOND)
			{
				printf("电锯: [%d,%d]位置[%s]资源不能切割.\r\n", dataPos.getX(), dataPos.getY(), (*it)->GetName().c_str());
				continue;
			}

			auto dataPos = preTile->GetDataPos();
			auto myDataPos = myTile->GetDataPos();

			(*it)->onHitBy((*it)->GetHP() / 2);
			printf("电锯: 己切割[%d,%d]位置[%s]资源, 剩余%d积分.\r\n", dataPos.getX(), dataPos.getY(), (*it)->GetName().c_str(), (*it)->GetHP());
			vecWorkRes.push_back(*it);
		}

		preDev->vecWorkRes.clear();
	}

	void Device::AddResourceScore(Tile* preTile)
	{
		if (preTile == nullptr)
			return;
		Device* preDev = preTile->GetDevice();
		if (preDev == nullptr || preDev->getType() != emDeviceType::DEV_BELT)
			return;
		if (preDev->vecWorkRes.size() <= 0)
			return;

		for (auto it = preDev->vecWorkRes.begin(); it != preDev->vecWorkRes.end(); it++)
		{
			auto dataPos = preTile->GetDataPos();

			printf("中心: 己将[%d,%d]位置[%s]资源交付中心, 得到%d点积分.\r\n", dataPos.getX(), dataPos.getY(), (*it)->GetName().c_str(), (*it)->GetHP());
			
			// 添加积分
			g_Game->AddScore((*it)->GetHP());
		}

		preDev->vecWorkRes.clear();
	}

	void Device::SetDevice(emDeviceType emType, emDeviceDir emDir, Position pos, int nHP)
	{
		m_emType = emType;
		m_emDir = emDir;
		m_pos = pos;
		SetHP(nHP);
		SetLifeType(LifeEntity::LF_DEVICE);
	}
}