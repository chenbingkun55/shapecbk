#pragma once
#include "Position.h"
#include "LifeEntity.h"
#include "Resource.h"
#include <vector>

namespace CBK
{
    class Tile;
    class Device : public LifeEntity
    {
    public:
        enum emDeviceType
        {
            DEV_MINER   = 1, // 开采器
            DEV_BELT    = 2, // 传送带
            DEV_CUTTER  = 3, // 电锯
            DEV_TRASH   = 4, // 垃圾桶
            DEV_CENTER  = 5, // 中心
            DEV_BUTTON  = 6, // 中心周边
        };

        enum emDeviceDir
        {
            NONE    = 0,
            TOP     = 1, // 上
            DOWN    = 2, // 下
            LEFT    = 3, // 左
            RIGHT   = 4, // 右

        };

        Device(emDeviceType emType, emDeviceDir emDir, Position pos, int nHP);
        ~Device();

        //获取
        emDeviceType getType();
        emDeviceDir getDir();

        void setType(emDeviceType type);

        bool onHitBy(int nDamage);

        void ClearThis();

        void show();

        void work();

        void BeltResource(Tile* preTile, Tile* myTile);
        void CutterResource(Tile* preTile, Tile* myTile, bool leftRight);
        void AddResourceScore(Tile* preTile);

        void SetDevice(emDeviceType emType, emDeviceDir emDir, Position pos, int nHP);

        vector<Resource*> vecWorkRes;

    private:
        emDeviceType m_emType;
        emDeviceDir m_emDir;
    };
}


