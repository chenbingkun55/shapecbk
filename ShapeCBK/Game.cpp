﻿#include <iostream>
#include "Game.h"
#include "GameConst.h"
#include "MapFile.h"

namespace CBK
{
	Game::Game()
	{
	}

	Game::~Game()
	{
	}

	bool Game::Initialize()
	{
		isInit = false;
		bGameOverFlag = false;

		mapFile = new MapFile();
		updateMgr = new UpdateManager();
        inputMgr = new InputManager();
		//map = new Map(MAP_SIZE);

		 //地图读取类
		if (mapFile->isInit() == false)
		{
			printf(">>> 读取地图文件失败...\r\n");
			return false;
		}

		return true;
	}

	void Game::InitData()
	{
		//初始化地图资源
		InitResource();

		isInit = true;
		bGameOverFlag = false;
	}


    void Game::InitResource()
    {
        // 资源物数量
        int nTileCount = 0;
        //xy坐标
        int x = 0;
        int y = 0;

        for (int row = 0; row < MAP_WIDTH; ++row)
        {
            for (int col = 0; col < MAP_HEIGHT; ++col)
            {
                nTileCount++;
            }
        }

        //分配空间
        if (tileCon.container_tile == nullptr)
        {
            tileCon.container_tile = new Tile * [nTileCount] { 0 };
        }
        else
        {
            Tile** pTile = tileCon.container_tile;
            int nCount = tileCon.nLength;

            for (int i = 0; i < nCount; i++)
            {
                if (pTile[i] != nullptr)
                {
                    delete pTile[i];
                }
            }
            delete[] tileCon.container_tile;
            tileCon.container_tile = new Tile * [nTileCount] { 0 };
        }
        tileCon.nLength = nTileCount;

        nTileCount = 0;
        Tile** pTile = tileCon.container_tile;

        //数据读取
        for (int row = 0; row < MAP_WIDTH; ++row)
        {
            for (int col = 0; col < MAP_HEIGHT; ++col)
            {
                // ID: 1020101

                //获取类型
                int tileId = g_MapData[row][col];

                int resType = GetResType(tileId);
                int resDir = GetResDir(tileId);
                int devType = GetDevType(tileId);
                int devDir = GetDevDir(tileId);

                //XY
                x = col * TILE_WIDTH;
                y = row * TILE_HEIGHT;

                if (tileId < 0)
                {
                    pTile[nTileCount++] = new Tile(nullptr, nullptr, { x, y }, TILE_LIFE);
                    continue;
                }

                Resource* pRes = GetResource(resType, resDir, x, y);
                Device* pDev = GetDevice(devType, devDir, x, y);

                pTile[nTileCount++] = new Tile(pRes, pDev, { x, y }, TILE_LIFE);

            }//forj
        }//fori
    }

    Resource* Game::GetResource(int resType, int resDir, int x, int y)
    {
        switch (resType)
        {
        case 1:
        {
            //添钻石
            return new Resource(Resource::RES_DIAMOND, (Resource::emResourceDir)resDir, { x, y }, RESOURCE_DIAMOND_LIFE);
        }
        case 2:
        {
            //添加铁
            return new Resource(Resource::RES_IRON, (Resource::emResourceDir)resDir, { x, y }, RESOURCE_IRON_LIFE);
        }
        }//switch Res

        return nullptr;
    }

    Device* Game::GetDevice(int devType, int devDir, int x, int y)
    {
        switch (devType)
        {
        case 1:
        {
            //开采器
            return new Device(Device::DEV_MINER, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        case 2:
        {
            //传送带
            return new Device(Device::DEV_BELT, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        case 3:
        {
            //电锯
            return new Device(Device::DEV_CUTTER, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        case 4:
        {
            //垃圾桶
            return new Device(Device::DEV_TRASH, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        case 5:
        {
            //中心
            return new Device(Device::DEV_CENTER, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        case 6:
        {
            //中心周边
            return new Device(Device::DEV_BUTTON, (Device::emDeviceDir)devDir, { x, y }, DEVICE_LIFE);
        }
        }//switch DEV

        return nullptr;
    }

    Tile* Game::GetTile(int row, int col)
    {
        int count = tileCon.nLength;
        for (size_t idx = 0; idx < count; ++idx)
        {
            Tile* tile = tileCon.container_tile[idx];
            Position dataPos = tile->GetDataPos();

            if (dataPos.getX() == row && dataPos.getY() == col)
                return tile;
        }

        return nullptr;
    }

    Tile* Game::GetTile(Position pos)
    {
        int count = tileCon.nLength;
        for (size_t idx = 0; idx < count; ++idx)
        {
            Tile* tile = tileCon.container_tile[idx];

            if (tile->GetPos()->getX() == pos.getX() && tile->GetPos()->getY() == pos.getY())
                return tile;
        }

        return nullptr;
    }

	bool Game::LoadMapData()
	{
		//读取地图数据
		return mapFile->getMap(g_MapData);
	}

	bool Game::InitMapData()
	{
		return mapFile->initMap(g_MapData);
	}

	void Game::Start()
	{
		GameLoop();	
	}

	bool Game::IsGameOver()
	{
		return bGameOverFlag;
	}


    int Game::GetResType(int tileId)
    {
        int resId = tileId % 1000;
        return resId / 100;
    }

    int Game::GetResDir(int tileId)
    {
        int resId = tileId % 1000;
        int dir = resId % 100;

        return dir > 0 ? dir : 0;
    }
    int Game::GetDevType(int tileId)
    {
        int devId = tileId / 10000;
        return devId / 100;
    }

    int Game::GetDevDir(int tileId)
    {
        int devId = tileId / 10000;
        int dir = devId % 100;

        return dir > 0 ? dir : 0;
    }

    int Game::GetTileId(int resType, int resDir, int devType, int devDir)
    {
        return devType * 1000000 + devDir * 10000 + resType * 100 + resDir;
    }

    wstring Game::GetDevKey(int emType, int emDir)
    {
        switch (emType)
        {
        case CBK::Device::DEV_MINER:
            switch (emDir)
            {
            case CBK::Device::TOP:
                return IMG_MINER_UP;
            case CBK::Device::DOWN:
                return IMG_MINER_DOWN;
            case CBK::Device::LEFT:
                return IMG_MINER_LEFT;
            case CBK::Device::RIGHT:
                return IMG_MINER_RIGHT;
            default:
                return IMG_ERROR;
            }
        case CBK::Device::DEV_BELT:
        {
            switch (emDir)
            {
            case CBK::Device::TOP:
                return IMG_BELT_UP;
            case CBK::Device::DOWN:
                return IMG_BELT_DOWN;
            case CBK::Device::LEFT:
                return IMG_BELT_LEFT;
            case CBK::Device::RIGHT:
                return IMG_BELT_RIGHT;
            default:
                return IMG_ERROR;
            }
        }
        break;
        case CBK::Device::DEV_CUTTER:
            switch (emDir)
            {
            case CBK::Device::TOP:
                return IMG_CUTTER_UP;
            case CBK::Device::DOWN:
                return IMG_CUTTER_DOWN;
            case CBK::Device::LEFT:
                return IMG_CUTTER_LEFT;
            case CBK::Device::RIGHT:
                return IMG_CUTTER_RIGHT;
            default:
                return IMG_ERROR;
            }
        case CBK::Device::DEV_TRASH:
            return IMG_TRASH;
        case CBK::Device::DEV_CENTER:
            switch (emDir)
            {
            case CBK::Device::TOP:
                return IMG_TC_CENTER_UP;
            case CBK::Device::DOWN:
                return IMG_TC_CENTER_DOWN;
            case CBK::Device::LEFT:
                return IMG_TC_CENTER_LEFT;
            case CBK::Device::RIGHT:
                return IMG_TC_CENTER_RIGHT;
            default:
                return IMG_ERROR;
            }
        case CBK::Device::DEV_BUTTON:
            return IMG_TC_BUTTON;
        default:
            return IMG_ERROR;
        }
    }

    wstring Game::GetResKey(int emType, int emDir)
    {
        switch (emType)
        {
        case CBK::Resource::RES_DIAMOND:
        {
            switch (emDir)
            {
            case CBK::Resource::TOP:
                return IMG_D_UP;
            case CBK::Resource::DOWN:
                return IMG_D_DOWN;
            case CBK::Resource::LEFT:
                return IMG_D_LEFT;
            case CBK::Resource::RIGHT:
                return IMG_D_RIGHT;
            default:
                return IMG_DIAMOND;
            }
        }
        break;
        case CBK::Resource::RES_IRON:
            return IMG_IRON;
        default:
            return IMG_TILE;
        }
    }

    void Game::PlantDevice(int row, int col, int devType, int devDir)
    {
        Tile* tile = GetTile(row, col);
        if (tile == nullptr)
            return;

        Position dataPos = tile->GetDataPos();
        int oldId = g_MapData[dataPos.getX()][dataPos.getY()];

        int oldResType = GetResType(oldId);
        int oldResDir = GetResDir(oldId);

        int x = col * TILE_WIDTH;
        int y = row * TILE_HEIGHT;

        Device* pDev = GetDevice(devType, devDir, x, y);
        tile->SetDevice(pDev);

        int newId = GetTileId(oldResType, oldResDir, devType, devDir);
        g_MapData[dataPos.getX()][dataPos.getY()] = newId;

        // 刷新地图
        //g_Ui->Clear();
        g_Ui->DrawMap();

        printf("放置成功。\r\n");
    }

    void Game::RemoveDevice(int row, int col)
    {
        Tile* tile = GetTile(row, col);
        if (tile == nullptr)
            return;

        Position dataPos = tile->GetDataPos();
        int oldId = g_MapData[dataPos.getX()][dataPos.getY()];

        int oldResType = GetResType(oldId);
        int oldResDir = GetResDir(oldId);

        tile->SetDevice(nullptr);

        int newId = GetTileId(oldResType, oldResDir, 0, 0);
        g_MapData[dataPos.getX()][dataPos.getY()] = newId;

        // 刷新地图
        //g_Ui->Clear();
        g_Ui->DrawMap();

        printf("删除成功。\r\n");
    }

    void Game::RotateDevice(int row, int col, int devDir)
    {
        Tile* tile = GetTile(row, col);
        if (tile == nullptr)
            return;

        Position dataPos = tile->GetDataPos();
        int oldId = g_MapData[dataPos.getX()][dataPos.getY()];

        int oldResType = GetResType(oldId);
        int oldResDir = GetResDir(oldId);

        int oldDevType = GetDevType(oldId);
        auto pDev = tile->GetDevice();
        if (pDev == nullptr)
        {
            printf("旋转失败, 未找到设备。\r\n");
            return;
        }

        int x = col * TILE_WIDTH;
        int y = row * TILE_HEIGHT;

        pDev->SetDevice((Device::emDeviceType)oldDevType, (Device::emDeviceDir)devDir, {x, y}, DEVICE_LIFE);

        int newId = GetTileId(oldResType, oldResDir, oldDevType, devDir);
        g_MapData[dataPos.getX()][dataPos.getY()] = newId;

        // 刷新地图
        //g_Ui->Clear();
        g_Ui->DrawMap();

        printf("旋转成功。\r\n");
    }

    void Game::DeviceWork()
    {
        //画瓦片
        int nLen = g_Game->tileCon.nLength;
        //瓦片容器
        Tile** pTile = g_Game->tileCon.container_tile;

        for (int i = 0; i < nLen; i++)
        {
            if (pTile[i] != nullptr && pTile[i]->isAlive())
            {
                Device* pDev = pTile[i]->GetDevice();
                if (pDev == nullptr)
                    continue;

                pDev->work();
            }
        }
    }

    void Game::AddScore(int score)
    {
        m_score += score;
    }
    void Game::PrintTotalScore()
    {
        printf("中心: 总计得到%d点积分.\r\n", m_score);
    }

    void Game::ExitGame()
    {
        bGameOverFlag = true;
    }
}
