﻿#pragma once

#include <string>
#include "GameUi.h"
#include "Map.h"
#include "UpdateManager.h"
#include "MapFile.h"
#include "Tile.h"
#include "InputManager.h"

namespace CBK
{
	class Game
	{
	public:
		// 瓦片容器
		struct TileContainer
		{
			Tile** container_tile;
			int nLength;
		};

	public:
		Game();
		~Game();

		bool Initialize();
		void InitData();
		void Start();

		bool InitMapData();
		void InitResource();
		Resource* GetResource(int resType, int resDir, int x, int y);
		Device* GetDevice(int devType, int devDir, int x, int y);
		bool LoadMapData();

		void GameLoop();
		void Update(float deltaTime) const;

		bool IsGameOver();

		Tile* GetTile(int row, int col);
		Tile* GetTile(Position pos);

		int GetResType(int tileId);
		int GetResDir(int tileId);
		int GetDevType(int tileId);
		int GetDevDir(int tileId);
		int GetTileId(int resType, int resDir, int devType, int devDir);

		// 获取图片KEY
		wstring GetDevKey(int emType, int emDir = 0);
		wstring GetResKey(int emType, int emDir = 0);


		// 操作
		void PlantDevice(int x, int y, int type, int dir = 0);
		void RemoveDevice(int x, int y);
		void RotateDevice(int x, int y, int devDir);
		void DeviceWork();

		void AddScore(int score);
		void PrintTotalScore();

		void ExitGame();

		Map* map;

		//容器
		TileContainer tileCon;

		UpdateManager* updateMgr;
		InputManager* inputMgr;

		int frameCount;

	private:
		bool isInit;

		int m_score;

		//游戏是否结束
		bool bGameOverFlag;

		//地图文件操作类
		MapFile* mapFile;
	};
}

