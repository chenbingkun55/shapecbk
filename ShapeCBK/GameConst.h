﻿#pragma once
#include "GameUi.h"
#include "Game.h"
#include "UpdateManager.h"

//窗口宽高(像素)
#define WINDOW_WIDTH   800
#define WINDOW_HEIGHT  800

// 瓦片宽高(像素)
#define TILE_WIDTH  40
#define TILE_HEIGHT  40

// 地图配置和数据
#define MAP_FILE_CONFIG   "./map.ini"
#define MAP_FILE_TEXT   "./map.txt"
#define MAP_FILE_DATA   "./map.dat"

//// 地图宽高(瓦片数量)
#define MAP_WIDTH  20
#define MAP_HEIGHT  20

//资源生命值
#define TILE_LIFE 2
#define RESOURCE_DIAMOND_LIFE 6
#define RESOURCE_IRON_LIFE 2
#define DEVICE_LIFE 2

//游戏所有数据全局指针
extern CBK::Game* g_Game;
extern CBK::GameUI* g_Ui;
//二维地图数据
extern int g_MapData[20][20];

//图片素材路径
#define IMG_BELT_LEFT TEXT("images\\belt_left.jpg")
#define IMG_BELT_UP TEXT("images\\belt_up.jpg")
#define IMG_BELT_RIGHT TEXT("images\\belt_right.jpg")
#define IMG_BELT_DOWN TEXT("images\\belt_down.jpg")
#define IMG_CUTTER_DOWN TEXT("images\\cutter_down.jpg")
#define IMG_CUTTER_LEFT TEXT("images\\cutter_left.jpg")
#define IMG_CUTTER_RIGHT TEXT("images\\cutter_right.jpg")
#define IMG_CUTTER_UP TEXT("images\\cutter_up.jpg")
#define IMG_DIAMOND TEXT("images\\diamond.jpg")
#define IMG_D_DOWN TEXT("images\\d_down.jpg")
#define IMG_D_LEFT TEXT("images\\d_left.jpg")
#define IMG_D_RIGHT TEXT("images\\d_right.jpg")
#define IMG_D_UP TEXT("images\\d_up.jpg")
#define IMG_IRON TEXT("images\\iron.jpg")
#define IMG_MINER_DOWN TEXT("images\\miner_down.jpg")
#define IMG_MINER_LEFT TEXT("images\\miner_left.jpg")
#define IMG_MINER_RIGHT TEXT("images\\miner_right.jpg")
#define IMG_MINER_UP TEXT("images\\miner_up.jpg")
#define IMG_SHOP TEXT("images\\shop.jpg")
#define IMG_TC_BUTTON TEXT("images\\tc_button.jpg")
#define IMG_TC_CENTER_UP TEXT("images\\tc_center_up.jpg")
#define IMG_TC_CENTER_DOWN TEXT("images\\tc_center_down.jpg")
#define IMG_TC_CENTER_LEFT TEXT("images\\tc_center_left.jpg")
#define IMG_TC_CENTER_RIGHT TEXT("images\\tc_center_right.jpg")
#define IMG_TRASH TEXT("images\\trash.jpg")
#define IMG_TILE TEXT("images\\tile.jpg")
#define IMG_ERROR TEXT("images\\error.jpg")