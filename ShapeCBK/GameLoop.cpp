﻿#include <iostream>
#include <chrono>
#include "Game.h"
#include "GameConst.h"

namespace CBK
{
	void Game::GameLoop()
	{
		using namespace std::chrono;

		frameCount = 0;
		auto previousTime = high_resolution_clock::now();
		auto currentTime = high_resolution_clock::now();
		duration<float> deltaTime = currentTime - previousTime;

		//清屏一次
		g_Ui->Clear();

		//显示地图
		g_Ui->DrawMap();


		while (!IsGameOver())
		{
			// 间隔时间
			currentTime = high_resolution_clock::now();
			deltaTime = currentTime - previousTime;
			previousTime = currentTime;

			Update(deltaTime.count());

			// 输入
			inputMgr->Update();

			g_Ui->DrawMap();

			// 帧数
			frameCount++;
			g_Ui->Wait(32);
		}

		g_Game->PrintTotalScore();
		cin.get();
	}

	void Game::Update(float deltaTime) const
	{
		updateMgr->Update(deltaTime);
	}
}
