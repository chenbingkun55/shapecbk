﻿#include "GameUi.h"
#include <graphics.h>
#include <ctime>
#include <sstream>
#include <tchar.h>
#include "GameConst.h"

namespace CBK
{
    GameUI::GameUI(int nWidth, int nHeight)
        : m_nWidth(nWidth), m_nHeight(nHeight)
    {
        //创建窗口
        initgraph(nWidth, nHeight, SHOWCONSOLE | NOCLOSE);

        //加载图片数据
        LoadImgData();
    }

    GameUI::~GameUI()
    {
    }

    void GameUI::LoadImgData()
    {
        loadimage(&mapImgObjects[IMG_BELT_LEFT], IMG_BELT_LEFT);
        loadimage(&mapImgObjects[IMG_BELT_UP], IMG_BELT_UP);
        loadimage(&mapImgObjects[IMG_BELT_RIGHT], IMG_BELT_RIGHT);
        loadimage(&mapImgObjects[IMG_BELT_DOWN], IMG_BELT_DOWN);
        loadimage(&mapImgObjects[IMG_CUTTER_DOWN], IMG_CUTTER_DOWN);
        loadimage(&mapImgObjects[IMG_CUTTER_LEFT], IMG_CUTTER_LEFT);
        loadimage(&mapImgObjects[IMG_CUTTER_RIGHT], IMG_CUTTER_RIGHT);
        loadimage(&mapImgObjects[IMG_CUTTER_UP], IMG_CUTTER_UP);
        loadimage(&mapImgObjects[IMG_DIAMOND], IMG_DIAMOND);
        loadimage(&mapImgObjects[IMG_D_DOWN], IMG_D_DOWN);
        loadimage(&mapImgObjects[IMG_D_LEFT], IMG_D_LEFT);
        loadimage(&mapImgObjects[IMG_D_RIGHT], IMG_D_RIGHT);
        loadimage(&mapImgObjects[IMG_D_UP], IMG_D_UP);
        loadimage(&mapImgObjects[IMG_IRON], IMG_IRON);
        loadimage(&mapImgObjects[IMG_MINER_DOWN], IMG_MINER_DOWN);
        loadimage(&mapImgObjects[IMG_MINER_LEFT], IMG_MINER_LEFT);
        loadimage(&mapImgObjects[IMG_MINER_RIGHT], IMG_MINER_RIGHT);
        loadimage(&mapImgObjects[IMG_MINER_UP], IMG_MINER_UP);
        loadimage(&mapImgObjects[IMG_SHOP], IMG_SHOP);
        loadimage(&mapImgObjects[IMG_TC_BUTTON], IMG_TC_BUTTON);
        loadimage(&mapImgObjects[IMG_TC_CENTER_UP], IMG_TC_CENTER_UP);
        loadimage(&mapImgObjects[IMG_TC_CENTER_DOWN], IMG_TC_CENTER_DOWN);
        loadimage(&mapImgObjects[IMG_TC_CENTER_LEFT], IMG_TC_CENTER_LEFT);
        loadimage(&mapImgObjects[IMG_TC_CENTER_RIGHT], IMG_TC_CENTER_RIGHT);
        loadimage(&mapImgObjects[IMG_TRASH], IMG_TRASH);
        loadimage(&mapImgObjects[IMG_TILE], IMG_TILE);
        loadimage(&mapImgObjects[IMG_ERROR], IMG_ERROR);

        isInit = true;
    }



    void GameUI::flush()
    {
        FlushBatchDraw();
    }

    void GameUI::Clear()
    {
        ClearRect(0, 0, m_nWidth, m_nHeight);
    }

    void GameUI::ClearRect(int x, int y, int width, int height)
    {
        clearrectangle(x, y, x + width, y + height);
    }

    void GameUI::DrawTile(int x, int y)
    {
        if (isInit == false) return;

        putimage(x, y, TILE_WIDTH, TILE_HEIGHT, &mapImgObjects[IMG_TILE], 0, 0);
    }

    void GameUI::DrawResource(int x, int y, int nType, int dir)
    {
        if (isInit == false) return;

        auto key = g_Game->GetResKey(nType, dir);
        putimage(x, y, TILE_WIDTH, TILE_HEIGHT, &mapImgObjects[key], 0, 0);
    }

    void GameUI::DrawDevice(int x, int y, int nType, int dir)
    {
        if (isInit == false) return;

        auto key = g_Game->GetDevKey(nType, dir);
        putimage(x, y, TILE_WIDTH, TILE_HEIGHT, &mapImgObjects[key], 0, 0);
    }

    void GameUI::DrawTextPos(int x, int y)
    {
        if (isInit == false) return;

        //TCHAR buf[5];
        //_itow(x, buf, 0);
        //_itow(',', buf, 1);
        //_itow(y, buf, 2);

        //outtextxy(x, y, buf);
    }

    void GameUI::DrawMap()
    {
        //画瓦片
        int nLen = g_Game->tileCon.nLength;
        //瓦片容器
        Tile** pTile = g_Game->tileCon.container_tile;

        for (int i = 0; i < nLen; i++)
        {
            if (pTile[i] != nullptr && pTile[i]->isAlive())
            {
                pTile[i]->show();
            }

        }
    }

    void GameUI::Wait(int time)
    {
        clock_t start = clock();
        clock_t end = start;
        do
        {
            end = clock();
        } while (end - start < time);
    }
}
