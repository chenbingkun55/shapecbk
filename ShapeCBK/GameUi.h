﻿#pragma once
#include <graphics.h>
#include <map>
#include <string>

using namespace std;

namespace CBK
{
	class GameUI
	{
	public:
		GameUI(int nWidth, int nHeight);
		~GameUI();

		void LoadImgData();

		void flush();

		void Clear();
		void ClearRect(int x, int y, int width, int height);

		void DrawTile(int x, int y);
		void DrawResource(int x, int y, int nType, int dir);
		void DrawDevice(int x, int y, int nType, int dir);
		void DrawTextPos(int x, int y);
		void DrawMap();

		void Wait(int time);

	public:
		bool isInit;

		map<wstring, IMAGE> mapImgObjects;

		//窗口宽高
		int m_nWidth;
		int m_nHeight;
	};
}

