#include "InputManager.h"
#include <iostream>
#include "windows.h"
#include <WinUser.h>
#include "GameConst.h"

using namespace std;

namespace CBK
{
	InputManager::InputManager()
	{
		mapOptType[emOptType::OPT_DeviceMenu] = make_unique<DeviceMenu>(this);
	}

	InputManager::~InputManager()
	{

	}

	bool InputManager::HandleInput()
	{
		PrintMenu();

		cout << "[主菜单]请选择：";
		cin >> opt;

		if (opt == 2)
		{
			return true;
		}

		if (opt == 3)
		{
			g_Game->DeviceWork();
			return true;
		}

		if (opt == 4)
		{
			g_Game->ExitGame();
			return true;
		}

		if (mapOptType.find((emOptType)opt) == mapOptType.end())
			return false;

		mapOptType[(emOptType)opt]->HandleInput();

		return true;
	}

	void InputManager::PrintMenu()
	{
		cout <<
			"1. 设备菜单\r\n"
			"2. 刷新地图\r\n"
			"3. 设备工作\r\n"
			"4. 退出\r\n"
			<< endl;

		cout << endl;
	}

	void InputManager::Update()
	{
		if (g_Game->IsGameOver())
			return;

		if (count > 20)
		{
			if (!HandleInput())
			{
				HandleInput();
			}

			count = 0;
		}

		count++;
		g_Ui->Wait(10);
	}

	DeviceMenu::DeviceMenu(InputManager* input)
	{
		inputMgr = input;
	}

	DeviceMenu::~DeviceMenu()
	{
		inputMgr = nullptr;
	}

	void DeviceMenu::HandleInput()
	{
		PrintMenu();

		cout << "[设备菜单] 请选择：";
		cin >> opt;

		switch ((emOptType)opt)
		{
		case CBK::DeviceMenu::OPT_PLANT:
		{
			int x, y;
			cout << "请输入设备坐标[x  y]：";
			cin >> x >> y;

			cout <<
				"1. 开采器\r\n"
				"2. 传送带\r\n"
				"3. 电锯\r\n"
				"4. 垃圾桶\r\n"
				<< endl;

			int dType;
			cout << "请输入设备类型: ";
			cin >> dType;

			cout <<
				"0. 无\r\n"
				"1. 上\r\n"
				"2. 下\r\n"
				"3. 左\r\n"
				"4. 右\r\n"
				<< endl;

			int devDir;
			cout << "请输入设备方向: ";
			cin >> devDir;

			g_Game->PlantDevice(x, y, dType, devDir);
		}
		break;
		case CBK::DeviceMenu::OPT_REMOVE:
		{
			int x, y;
			cout << "请输入设备坐标[x  y]：";
			cin >> x >> y;

			g_Game->RemoveDevice(x, y);
		}
			break;
		case CBK::DeviceMenu::OPT_ROTATE:
		{
			int x, y;
			cout << "请输入设备坐标[x  y]：";
			cin >> x >> y;

			cout <<
				"0. 无\r\n"
				"1. 上\r\n"
				"2. 下\r\n"
				"3. 左\r\n"
				"4. 右\r\n"
				<< endl;

			int devDir;
			cout << "请输入设备方向: ";
			cin >> devDir;

			g_Game->RotateDevice(x, y, devDir);
		}
			break;
		case CBK::DeviceMenu::OPT_MainMenu:
		default:
			inputMgr->HandleInput();
			break;
		}

		HandleInput();
	}
	void DeviceMenu::PrintMenu()
	{
		cout <<
			"0. 返回主菜单\r\n"
			"1. 放置设备\r\n"
			"2. 旋转设备\r\n"
			"3. 删除设备\r\n"
			<< endl;

		cout << endl;
	}
}
