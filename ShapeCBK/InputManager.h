#pragma once
#include <map>
#include <memory>

using namespace std;

namespace CBK
{
	class InputManager;

	class MenuBase
	{
	public:
		virtual void HandleInput() = 0;
		virtual void PrintMenu() = 0;
	};

	class DeviceMenu : public MenuBase
	{
	public:
		enum emOptType
		{
			OPT_MainMenu = 0, // 返回主菜单
			OPT_PLANT = 1, // 设备放置
			OPT_ROTATE = 2, // 设备转向
			OPT_REMOVE = 3, // 设备删除
		};
	public:
		DeviceMenu(InputManager* inputMgr);
		~DeviceMenu();

		void HandleInput() override;
		void PrintMenu() override;

	private:
		int opt = 0;
		InputManager* inputMgr;
	};

	// ------------------------------------------------------------------------------------------------
	class InputManager
	{
	public:
		enum emOptType
		{
			OPT_DeviceMenu	= 1, // 设备菜单（放置，删除，转向）
		};

		InputManager();
		~InputManager();

		bool HandleInput();
		void PrintMenu();

		void Update();

	private:
		int count = 0;
		int opt = 0;

		emOptType optType = emOptType::OPT_DeviceMenu;

		map<emOptType, unique_ptr<MenuBase>> mapOptType;

	};

}

