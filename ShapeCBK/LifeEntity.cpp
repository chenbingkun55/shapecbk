#include "LifeEntity.h"

namespace CBK
{
    LifeEntity::LifeEntity(Position pos, emLifeType emType, int nHP)
        : m_pos(pos), m_emType(emType), m_nHP(nHP)
    {
        m_bAliveFlag = true;
    }

    LifeEntity::~LifeEntity()
    {

    }

    void LifeEntity::toDie()
    {
        m_bAliveFlag = false;
    }

    bool LifeEntity::isAlive()
    {
        return m_bAliveFlag;
    }

    bool LifeEntity::onHitBy(int nDamage)
    {
        return false;
    }

    void LifeEntity::show()
    {
    }

    int LifeEntity::GetHP()
    {
        return m_nHP;
    }

    void LifeEntity::SetHP(int nHP)
    {
        m_nHP = nHP;
    }

    bool LifeEntity::SubHp(int nHP)
    {
        m_nHP = m_nHP - nHP;
        if (m_nHP <= 0)
        {
            m_nHP = 0;
            return false;
        }
        return true;
    }

    void LifeEntity::toAlive()
    {
        m_bAliveFlag = true;
    }

    void LifeEntity::SetLifeType(emLifeType emType)
    {
        m_emType = emType;
    }

    LifeEntity::emLifeType LifeEntity::GetLifeType()
    {
        return m_emType;
    }

    Position* LifeEntity::GetPos()
    {
        return &m_pos;
    }
}
