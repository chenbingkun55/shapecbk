#pragma once
#include "Position.h"

namespace CBK
{
	class LifeEntity
	{
	public:
		enum emLifeType
		{
			LF_TILE			= 1, // 瓦片
			LF_RESOURCE		= 2, // 资源
			LF_DEVICE		= 3, // 设备
		};

	public:
		LifeEntity(Position m_pos, emLifeType emType, int HP);

		~LifeEntity();

		bool SubHp(int nHP);

		//复活
		void toAlive();

		//死亡
		void toDie();

		//是否还活着
		bool isAlive();

		//被开采中
		virtual bool onHitBy(int nDamage);

		//显示
		virtual void show();

		//获取生命值
		int GetHP();
		void SetHP(int nHP);

		//设置类型
		void SetLifeType(emLifeType m_emType);
		emLifeType GetLifeType();

		Position* GetPos();

	public:
		//坐标
		Position m_pos;
	private:
		//生命值
		int m_nHP;
		//生命体类型
		emLifeType m_emType;
		//是否还活着
		bool m_bAliveFlag;
	};
}

