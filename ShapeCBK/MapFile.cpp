#include "MapFile.h"
#include "GameConst.h"
#include <sstream>
#pragma warning(disable:4996)

using namespace std;

namespace CBK
{
	MapFile::MapFile()
	{
		FILE* fp = fopen(MAP_FILE_CONFIG, "rb+");
		char buf[8] = { 0 };

		if (fp == NULL)
		{
			m_bInitFlag = false;
			goto EXIT;
		}

		//读一行
		fgets(buf, 8, fp);
		//游戏速度
		m_nGameSpeed = atoi(buf);

		m_bInitFlag = true;

	EXIT:
		if (fp != NULL)
		{
			fclose(fp);
		}
		//初始化
		m_fp = NULL;
	}


	MapFile::~MapFile()
	{
	}

	bool MapFile::isInit()
	{
		return m_bInitFlag;
	}

	bool MapFile::open(const char* pszFileName, emMode mode)
	{
		close();
		if (mode == MD_READ)
		{
			m_fp = fopen(pszFileName, "rb");
		}
		else if (mode == MD_WRITE)
		{
			m_fp = fopen(pszFileName, "wb");
		}
		return m_fp != NULL;
	}

	void MapFile::close()
	{
		if (m_fp != NULL)
		{
			fclose(m_fp);
			m_fp = NULL;
		}
	}

	bool MapFile::initMap(int mapdate[20][20])
	{
		bool ret = false;
		std::ifstream inputFile;
		inputFile.open(MAP_FILE_TEXT);

		if (inputFile.is_open())
		{
			string line;
			for (size_t row = 0; row < MAP_WIDTH; ++row)
			{
				int col = 0;
				getline(inputFile, line);

				std::stringstream ss(line);
				vector<string> vecCols = stringSplit(line, '\t');

				for (size_t i = 0; i < vecCols.size(); ++i)
				{
					int type = atoi(vecCols[i].c_str());
					mapdate[row][col] = type;
					col++;
				}
			}

			ret = true;
		}
		else 
		{
			printf(">>> ERROR: 读取地图初始化数据出错...\r\n");
		}

		inputFile.close();

		return ret;
	}

	std::vector<std::string> MapFile::stringSplit(const std::string& str, char delim)
	{
		std::stringstream ss(str);
		std::string item;
		std::vector<std::string> elems;
		while (std::getline(ss, item, delim)) {
			if (!item.empty()) {
				elems.push_back(item);
			}
		}
		return elems;
	}

	bool MapFile::getMap(int mapdate[20][20])
	{
		bool ret = false;

		//打开文件
		if (open(MAP_FILE_DATA, MD_READ))
		{
			//读取数据
			if (fread(mapdate, 20 * 20, 1, m_fp) == 1)
			{
				ret = true;
			}
		}
		//关闭文件
		close();

		return ret;
	}

	bool MapFile::setMap(int mapdate[20][20])
	{
		bool ret = false;

		//打开文件
		if (open(MAP_FILE_DATA, MD_WRITE))
		{
			//读取数据
			if (fwrite(mapdate, 20 * 20, 1, m_fp) == 1)
			{
				ret = true;
			}
		}
		//关闭文件
		close();
		return ret;
	}

	int MapFile::getGameSpeed()
	{
		return m_nGameSpeed;
	}
}