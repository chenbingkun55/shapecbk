#pragma once
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace CBK
{
	class MapFile
	{
	public:
		MapFile();
		~MapFile();

		//模式
		enum emMode
		{
			MD_READ,
			MD_WRITE
		};

		//是否初始化
		bool isInit();

		//打开文件
		bool open(const char* pszFileName, emMode mode);

		//关闭文件
		void close();

		// 初始化地图数据
		bool initMap(int mapdate[20][20]);
		std::vector<std::string> stringSplit(const std::string& str, char delim);

		//读取地图数据
		bool getMap(int mapdate[20][20]);

		//写地图数据
		bool setMap(int mapdate[20][20]);

		//读取游戏速度
		int getGameSpeed();

	private:
		FILE* m_fp;
		int m_nGameSpeed;
		bool m_bInitFlag;
	};
}

