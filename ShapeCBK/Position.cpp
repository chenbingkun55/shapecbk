#include "Position.h"

namespace CBK
{
	Position::Position()
	{
		x = 0;
		y = 0;
	}

	Position::Position(int x, int y)
		: x(x), y(y)
	{
	}

	Position::~Position()
	{
	}

	bool Position::toLeft(int nStep = 1)
	{
		int nX = x - nStep; //��
		if (nX < 0)
		{
			x = 0;
		}
		else
		{
			x = nX;
		}
		return true;
	}

	bool Position::toRight(int nStep = 1)
	{
		x = x + nStep;
		return true;
	}

	bool Position::toDown(int nStep = 1)
	{
		y = y + nStep; //��
		return true;
	}

	void Position::setPos(int x, int y)
	{
		setX(x);
		setY(y);
	}

	void Position::setX(int x)
	{
		this->x = x;
	}

	int Position::getX()
	{
		return x;
	}

	void Position::setY(int y)
	{
		this->y = y;
	}

	int Position::getY()
	{
		return y;
	}

	bool Position::toUp(int nStep = 1)
	{
		int nY = y - nStep;
		if (nY < 0)
		{
			y = 0;
		}
		else
		{
			y = nY;
		}

		return true;
	}
}
