#include "Resource.h"
#include "GameConst.h"

using namespace std;

namespace CBK
{
    Resource::Resource(emResourceType emType, emResourceDir emDir, Position pos, int nHP)
        : m_emType(emType), m_emDir(emDir), LifeEntity(pos, LifeEntity::LF_RESOURCE, nHP)
    {
    }

    string Resource::GetName()
    {
        switch (m_emType)
        {
        case CBK::Resource::RES_DIAMOND:
            return "钻石";
        case CBK::Resource::RES_IRON:
            return "铁矿";
        default:
            return "未知矿";
            break;
        }
    }

    //获取类型
    Resource::emResourceType Resource::getType()
    {
        return m_emType;
    }

    Resource::emResourceDir Resource::getDir()
    {
        return m_emDir;
    }

    void Resource::setType(emResourceType type)
    {
        m_emType = type;
    }

    bool Resource::onHitBy(int nDamage)
    {

        if (SubHp(nDamage) == false)
        {
            //清除显示
            ClearThis();

            //调用死亡
            toDie();
            return true;
        }
        else
        {
            show();
            g_Ui->flush();
        }

        return false;
    }

    void Resource::ClearThis()
    {
        g_Ui->ClearRect(m_pos.getX(), m_pos.getY(), TILE_WIDTH, TILE_HEIGHT);
    }

    void Resource::show()
    {
        g_Ui->DrawResource(m_pos.getX(), m_pos.getY(), m_emType, m_emDir);
    }

    //设置数据
    void Resource::SetResource(emResourceType emType, emResourceDir emDir,  Position pos, int nHP)
    {
        m_emType = emType;
        m_emDir = emDir;
        m_pos = pos;
        SetHP(nHP);
        SetLifeType(LifeEntity::LF_RESOURCE);
    }

    Resource::~Resource()
    {
    }

}