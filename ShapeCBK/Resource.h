#pragma once
#include "Position.h"
#include "LifeEntity.h"
#include <string>

using namespace std;

namespace CBK
{
	class Resource : public LifeEntity
	{
    public:
        enum emResourceType
        {
            RES_DIAMOND     = 1, // 钻石
            RES_IRON        = 2, // 铁
        };

        enum emResourceDir
        {
            NONE    = 0,
            TOP     = 1, // 上
            DOWN    = 2, // 下
            LEFT    = 3, // 左
            RIGHT   = 4, // 右
            
        };

        Resource(emResourceType emType, emResourceDir emDir, Position pos, int nHP = 1);
        ~Resource();

        //获取
        string GetName();
        emResourceType getType();
        emResourceDir getDir();

        void setType(emResourceType type);

        bool onHitBy(int nDamage);

        void ClearThis();

        void show();

        void SetResource(emResourceType emType, emResourceDir emDir, Position pos, int nHP);

    private:
        emResourceType m_emType;
        emResourceDir m_emDir;
	};
}

