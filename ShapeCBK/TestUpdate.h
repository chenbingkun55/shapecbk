﻿#pragma once
#include "Core/IUpdate.h"

namespace CBK
{
	class TestUpdate : public IUpdate
	{
	public:
		TestUpdate();
		~TestUpdate() override;

		void OnUpdate(float deltaTime) const override;

	};
}

