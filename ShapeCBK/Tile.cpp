#include "Tile.h"
#include "GameConst.h"

namespace CBK
{
	Tile::Tile(Resource* res, Device* dev, Position pos, int nHP)
		: m_pRes(res), m_pDev(dev), LifeEntity(pos, LifeEntity::LF_TILE, nHP)
	{
	}

	Tile::~Tile()
	{
	}

	Position Tile::GetDataPos()
	{
		auto pos = GetPos();
		return { pos->getX() / TILE_WIDTH , pos->getY() / TILE_HEIGHT };
	}

	Resource* Tile::GetResource()
	{
		return m_pRes;
	}

	Device* Tile::GetDevice()
	{
		return m_pDev;
	}

	void Tile::show()
	{
		g_Ui->DrawTile(m_pos.getX(), m_pos.getY());

		if (m_pRes != nullptr)
			m_pRes->show();
		if (m_pDev != nullptr)
			m_pDev->show();
	}
	void Tile::SetDevice(Device* dev)
	{
		if (m_pDev != nullptr)
			delete m_pDev;

		m_pDev = dev;
	}

	void Tile::SetResource(Resource* res)
	{
		if (m_pRes != nullptr)
			delete m_pRes;

		m_pRes = res;
	}

	void Tile::MinerResource()
	{
		m_pRes = nullptr;
	}
}
