#pragma once
#include "Device.h"
#include "Position.h"
#include "Resource.h"
#include "LifeEntity.h"

namespace CBK
{
    class Tile : public LifeEntity
    {
    public:
        Tile(Resource* res, Device* dev, Position pos, int nHP);
        ~Tile();

        Position GetDataPos();
        Resource* GetResource();
        void MinerResource();
        Device* GetDevice();

        void show();

        // �����豸
        void SetDevice(Device* dev);
        void SetResource(Resource* res);

    private:
        Resource* m_pRes;
        Device* m_pDev;

    };

}

