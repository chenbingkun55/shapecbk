﻿#include "UpdateManager.h"

namespace CBK
{
	UpdateManager::UpdateManager()
	{
	}

	UpdateManager::~UpdateManager()
	{
		m_vecUpdates.clear();
	}

	bool UpdateManager::Register(std::unique_ptr<IUpdate> pUpdate)
	{
		auto it = std::find(m_vecUpdates.begin(), m_vecUpdates.end(), pUpdate);
		if (it != m_vecUpdates.end())
			return false;
		
		m_vecUpdates.emplace_back(std::move(pUpdate));

		return true;
	}

	bool UpdateManager::UnRegister(std::unique_ptr<IUpdate> pUpdate)
	{
		auto it = std::find(m_vecUpdates.begin(), m_vecUpdates.end(), pUpdate);
		if (it == m_vecUpdates.end())
			return false;

		m_vecUpdates.erase(it);

		return true;
	}

	void UpdateManager::Update(float deltaTime)
	{
		for (auto it = m_vecUpdates.begin(); it != m_vecUpdates.end(); ++it)
		{
			(*it)->OnUpdate(deltaTime);
		}
	}
}
