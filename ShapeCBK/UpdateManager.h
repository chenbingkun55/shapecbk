﻿#pragma once

#include <vector>
#include <memory>
#include "Core/IUpdate.h"

namespace CBK
{
	class UpdateManager
	{
	public:
		UpdateManager();
		~UpdateManager();

		bool Register(std::unique_ptr<IUpdate> pUpdate);
		bool UnRegister(std::unique_ptr<IUpdate> pUpdate);

		void Update(float deltaTime);

	private:
		std::vector<std::unique_ptr<IUpdate>> m_vecUpdates;
	};
}

