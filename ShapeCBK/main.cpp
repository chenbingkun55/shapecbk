﻿#include <iostream>
#include "Game.h"
#include "GameConst.h"
#include "TestUpdate.h"

using namespace CBK;
using namespace std;

int main()
{
	g_Ui = new GameUI(WINDOW_WIDTH, WINDOW_HEIGHT);
	g_Game = new Game();

	while (true)
	{
		if (g_Game->Initialize() == false)
		{
			break;
		}

		//auto testUpdate = make_unique<TestUpdate>();
		//g_Game->updateMgr->Register(std::move(testUpdate));

		//读取地图数据
		if (g_Game->LoadMapData() == false)
		{
			g_Game->InitMapData();
		}

		//初始化数据
		g_Game->InitData();

		//开始游戏
		g_Game->Start();
		break;
	}
}